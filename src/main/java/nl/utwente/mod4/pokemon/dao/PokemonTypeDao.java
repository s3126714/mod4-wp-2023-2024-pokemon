package nl.utwente.mod4.pokemon.dao;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.NotSupportedException;
import nl.utwente.mod4.pokemon.Utils;
import nl.utwente.mod4.pokemon.model.PokemonType;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;

public enum PokemonTypeDao {

    INSTANCE;

    private static final String ORIGINAL_POKEMON_TYPES = Utils.getAbsolutePathToResources() + "/aggregated-and-filtered-pokemon-dataset.json";
    private static final String POKEMON_TYPES = Utils.getAbsolutePathToResources() + "/pokemon-types.json";

    private HashMap<String, PokemonType> pokemonTypes = new HashMap<>();

    public void delete(String id) {
        if(pokemonTypes.containsKey(id)) {
            pokemonTypes.remove(id);
        } else {
            throw new NotFoundException("Pokemon type '" + id + "' not found.");
        }
    }

    public List<PokemonType> getPokemonTypes(int pageSize, int pageNumber, String sortBy) {
        List<PokemonType> list = new ArrayList<>(pokemonTypes.values());

        if (sortBy == null || sortBy.isEmpty() || "id".equals(sortBy))
            list.sort((pt1, pt2) -> Utils.compare(Integer.parseInt(pt1.id), Integer.parseInt(pt2.id)));
        else if ("pokedexNumber".equals(sortBy))
            list.sort((pt1, pt2) -> Utils.compare(pt1.pokedexNumber, pt2.pokedexNumber));
        else if ("lastUpDate".equals(sortBy))
            list.sort((pt1, pt2) -> Utils.compare(pt1.lastUpDate, pt2.lastUpDate));
        else
            throw new NotSupportedException("Sort field not supported");

        return (List<PokemonType>) Utils.pageSlice(list,pageSize,pageNumber);
    }

    public PokemonType getPokemonType(String id) {
        var pt = pokemonTypes.get(id);

        if (pt == null) {
            throw new NotFoundException("Pokemon '" + id + "' not found!");
        }

        return pt;
    }

    public void load() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File source = existsPokemonTypes() ?
                new File(POKEMON_TYPES) :
                new File(ORIGINAL_POKEMON_TYPES);
        PokemonType[] arr = mapper.readValue(source, PokemonType[].class);

        Arrays.stream(arr).forEach(pt -> pokemonTypes.put(pt.id, pt));
    }

    private boolean existsPokemonTypes() {
        File f = new File(POKEMON_TYPES);
        return f.exists() && !f.isDirectory();
    }

    public void save() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        File destination = new File(POKEMON_TYPES);

        writer.writeValue(destination, pokemonTypes.values());
    }

    public PokemonType create(PokemonType newPokemonType) {
        String nextId = "" + (getMaxId() + 1);

        newPokemonType.id = nextId;
        newPokemonType.created = Instant.now().toString();
        newPokemonType.lastUpDate = Instant.now().toString();
        pokemonTypes.put(nextId,newPokemonType);

        return newPokemonType;
    }

    private int getMaxId() {
        Set<String> ids = pokemonTypes.keySet();
        return ids.isEmpty() ? 0 : ids.stream()
                .map(Integer::parseInt)
                .max(Integer::compareTo)
                .get();
    }

    public PokemonType update(PokemonType updated) {
        if(!updated.isValid())
            throw new BadRequestException("Invalid pokemon type.");
        if(pokemonTypes.get(updated.id) == null)
            throw new NotFoundException("Pokemon type id '" + updated.id + "' not found.");

        updated.lastUpDate = Instant.now().toString();
        pokemonTypes.put(updated.id,updated);

        return updated;
    }

    public int getTotalPokemonTypes() {
        return pokemonTypes.keySet().size();
    }
}
